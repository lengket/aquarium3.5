package es.ava.aruco;

import android.content.Context;
import android.support.annotation.CallSuper;
import android.util.Log;
import android.view.MotionEvent;

import org.opencv.core.Mat;
import org.opencv.samples.tutorial1.R;
import org.rajawali3d.Object3D;
import org.rajawali3d.lights.DirectionalLight;
import org.rajawali3d.loader.ParsingException;
import org.rajawali3d.materials.Material;
import org.rajawali3d.materials.MaterialManager;
import org.rajawali3d.materials.methods.DiffuseMethod;
import org.rajawali3d.materials.textures.Texture;
import org.rajawali3d.math.Quaternion;
import org.rajawali3d.primitives.Cube;
import org.rajawali3d.renderer.ISurfaceRenderer;
import org.rajawali3d.view.IDisplay;
import org.rajawali3d.view.SurfaceView;
import org.rajawali3d.renderer.Renderer;
import org.rajawali3d.loader.LoaderOBJ;

public class CubeRenderer implements IDisplay{
    protected TransparentSurfaceRenderer renderer;
    protected Context con;
    protected ISurfaceRenderer mRenderer;

    public CubeRenderer(Context con){
        this.renderer=new TransparentSurfaceRenderer(con);
        this.con=con;
        mRenderer=createRenderer();

    }

    @CallSuper
    public void applyRenderer(SurfaceView mRenderSurface) {
        mRenderSurface.setSurfaceRenderer(mRenderer);
    }

    public void onBeforeApplyRenderer(SurfaceView mRenderSurface) {
        mRenderSurface.setTransparent(true);
    }

    public void setRotation(Quaternion rotation){
        renderer.setRotation(rotation);
    }

    public void setPosition(Mat Tvec){
        renderer.setCameraPosition(Tvec.get(0,0)[0]*-200,Tvec.get(1,0)[0]*200,Tvec.get(2,0)[0]);
    }
    public void setRotationNQ(Mat Rvec){
        renderer.setRotationNonQ(Rvec);
    }

    @Override
    public TransparentSurfaceRenderer createRenderer() {
        return this.renderer;
    }

    private final class TransparentSurfaceRenderer extends Renderer {
        Object3D monkey;

        TransparentSurfaceRenderer(Context context) {
            super(context);
        }

        public void set3DObjectPosition(double x, double y, double z) {

            if (monkey != null)
                monkey.setPosition(x, y, z);
        }
        public void setRotation(Quaternion rotation){
            rotation = rotation.inverse();
            monkey.setRotation(rotation);
        }

        public void setRotationNonQ(Mat Rvec){
            getCurrentCamera().setRotX(Rvec.get(1,0)[0]);
            getCurrentCamera().setRotY(Rvec.get(0,0)[0]);
            getCurrentCamera().setRotZ(Rvec.get(2,0)[0]);

        }

        public void setCameraPosition(double x, double y, double z) {

            getCurrentCamera().setX(x);
            getCurrentCamera().setY(y);
            getCurrentCamera().setZ(z);
        }


        @Override
        protected void initScene() {
            DirectionalLight light = new DirectionalLight(0, 0, -1);
            light.setPower(1);
            getCurrentScene().addLight(light);
            getCurrentCamera().setPosition(0, 0, 0);
            MaterialManager materialManager = MaterialManager.getInstance();

            try {
                Material material = new Material();
                Texture owo = new Texture("owo",R.drawable.aquarium_difuse);
                material.enableLighting(true);
                material.setDiffuseMethod(new DiffuseMethod.Lambert());
                LoaderOBJ loader=new LoaderOBJ(this,R.raw.aquarium_obj);
                material.addTexture(owo);
                material.setCurrentObject(loader.getParsedObject());
            try {
                loader.parse();
            } catch (ParsingException e) {
                e.printStackTrace();
            }
//            monkey = loader.getParsedObject();
//                for(int i=0; i<monkey.getNumChildren(); i++) {
//                    Material oldMaterial = monkey.getChildAt(i).getMaterial();
//                    materialManager.removeMaterial(oldMaterial);
//                }
                monkey = new Cube(2.0f);
                monkey.setMaterial(material);

                monkey.setScale(2);
                getCurrentScene().addChild(monkey);

            } catch (Exception e) {
                e.printStackTrace();
                Log.d("Renderer",e.getMessage());
            }

            // -- set the background color to be transparent
            // you need to have called setGLBackgroundTransparent(true); in the activity
            // for this to work.
            getCurrentScene().setBackgroundColor(0);
        }

        @Override
        public void onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {

        }

        @Override
        public void onTouchEvent(MotionEvent event) {

        }

    }
}
